function sumar(numero1, numero2) {
  return parseInt(numero1) + parseInt(numero2);
}

function restar(numero1, numero2) {
  return numero1 - numero2;
}

function multiplicar(numero1, numero2) {
  return numero1 * numero2;
}

function dividir(numero1, numero2) {
  return numero1 / numero2;
}

function fresult(flag) {

  let resultadoTotal
  let num1 = document.getElementById("idAcumulado").textContent;
  let num2 = document.getElementById("inputNum").value;
  let operator = document.getElementById("IDoperador").textContent;

  if (num1 != '' && num2 != '' && operator != '') {

    if (operator == "+") {
      resultadoTotal = sumar(num1, num2);
    } else if (operator == "-") {
      resultadoTotal = restar(num1, num2);
    } else if (operator == "*") {
      resultadoTotal = multiplicar(num1, num2);
    } else if (operator == "/") {
      resultadoTotal = dividir(num1, num2);
    }

    if (flag) {

      document.getElementById('inputNum').value = resultadoTotal;
      document.getElementById('idAcumulado').innerText = '';
      document.getElementById('IDoperador').innerText = '';

    } else {
      document.getElementById('idAcumulado').innerText = resultadoTotal;
    }

  } else {
    console.log('Faltan datos por ingresar');
  }
}

function borrarNumero() {
  
  let inputNumero = document.getElementById('inputNum');
  let valorActual = inputNumero.value;
  let valorBorrado = valorActual.slice(0, -1);
  let inputOperador = document.getElementById('IDoperador');  
  let valorArriba = document.getElementById('idAcumulado');

  if (valorActual != "") {
    inputNumero.value = valorBorrado;
  } else if (valorActual == "" && inputOperador.textContent != ""){
    inputOperador.innerText = "";
    
  } else{
    document.getElementById('inputNum').value = valorArriba.textContent;
    valorArriba.innerText = "";
  }
}